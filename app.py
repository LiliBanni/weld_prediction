import flask
from flask import Flask, render_template, request
import numpy as np
import joblib
import sklearn
from sklearn.multioutput import MultiOutputRegressor
from sklearn import tree
from sklearn.preprocessing import MinMaxScaler


app = Flask(__name__)

model_weld = joblib.load('model_weld.pkl')
normal = joblib.load('normal.pkl')


@app.route('/', methods=["get", "post"])
def predict():
    message = ""
    if request.method == "POST":
        iw = float(flask.request.form.get("iw"))
        if_ = float(flask.request.form.get("if_"))
        vw = float(flask.request.form.get("vw"))
        fp = float(flask.request.form.get("fp"))

        scaler = MinMaxScaler()
        scaler.min_ = normal['min']
        scaler.scale_ = normal['scale']

        input_data = [[iw, if_, vw, fp]]

        print('\n Введенные данные', input_data, '\n')

        input_data = scaler.transform(input_data)
        print('\n Нормализованные данные', input_data, '\n')

        predicted_results = model_weld.predict(input_data)
        print('\n Предсказанные значения', predicted_results, '\n')

        depth = round(predicted_results[0][0], 2)
        width = round(predicted_results[0][1], 2)

        message = f"ПОЛУЧЕНЫ РЕЗУЛЬТАТЫ: Глубина шва (Depth): {depth}; Ширина шва (Width): {width}"

    return render_template("index.html", message=message)


app.run()
